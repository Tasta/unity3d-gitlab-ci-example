#!/usr/bin/env bash

echo "Registering license..."

export BUILD_PATH=./Builds/$BUILD_TARGET/
mkdir -p $BUILD_PATH

# Run Unity Once to Setup the License
/opt/Unity/Editor/Unity \
  -projectPath /root/project \
  -username $UNITY_USERNAME \
  -password $UNITY_PASSWORD \
  -manualLicenseFile Unity_v2019.x.ulf \
  -batchmode \
  -quit \
  -nographics \
  -logFile licensing.log
  
echo "License registered!"